//
//  ScoreManager.swift
//  Simple
//
//  Created by Anton Ivanov on 29.12.15.
//  Copyright © 2015 TonyCo. All rights reserved.
//

import Foundation
import UIKit


public class ScoreManager {
    
    private var currentScore: Int = 0
    private var scoreLabel: UILabel?

    
    init(textView: UILabel) {
        scoreLabel = textView
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            while true{
                sleep(50)
                if self.currentScore < 0 {
                    self.currentScore = 0
                }
                textView.text = String(self.currentScore)
            }
        })
    }
    

    public func addScore(score: Int) {
            self.currentScore += score
    }
    
    internal func deleteScore(score: Int){
        if (self.currentScore > score){
            self.currentScore -= score
        }
        else{
            currentScore = 0
        }
        scoreLabel?.text = "" + String(self.currentScore)

    }
    
}