//
//  CreateCircleButton.swift
//  Simple
//
//  Created by Anton Ivanov on 23.12.15.
//  Copyright © 2015 TonyCo. All rights reserved.
//

import Foundation
import UIKit

class CreateCircleButton {
    
    internal var i:Int = 0

    var mcircleList: QArray
    private var circleButton: CircleButton?
    let height, width: Int?
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    
    
    init(view: ViewController, circleList: QArray){
        
        let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)
        self.mcircleList = circleList
        
        height = Int(self.screenSize.height - CGFloat(CircleButton.MINRAD)*2)
        width = Int(self.screenSize.width - CGFloat(CircleButton.MINRAD)*2)

        dispatch_async(backgroundQueue, {
            print("Create the new thread in CreateCircleButton for create random coordinates for new circle")
            var creatingNewCircleOnNextIteration = 1
            while true {
                if (creatingNewCircleOnNextIteration == 1){
                    
                    let randX = Int(arc4random_uniform(UInt32(self.width!)) + UInt32(CircleButton.MINRAD))
                    let randY = Int(arc4random_uniform(UInt32(self.height!)) + UInt32(CircleButton.MINRAD))
                    var resume = false
                    
                    for circle in circleList.newArray{
                        let dx = abs(randX - circle.getx())
                        let dy = abs(randY - circle.gety())
                        let indent = Int(circle.getRadius() + CircleButton.MINRAD)
                        if (dx * dx + dy * dy < indent*indent ){
                            resume = true
                            break
                        }
                    }
                    
                    if (resume){
                        continue
                    }
                    
                    let circles: QArray = circleList
                    var minSpace = CircleButton.MAXRAD
                    
                    for circle in circles.newArray {
                        let dx = Double(abs(randX - circle.getx()))
                        let dy = Double(abs(randY - circle.gety()))
                        let space = Int(sqrt(Double(dx*dx+dy*dy)) - Double(circle.getRadius()))
                        if (space < CircleButton.MINRAD){
                            resume = true
                            break
                        }
                        if (space < minSpace){ minSpace = space }
                    }
                    if (resume){ continue }
                    
                    var radius = Int(arc4random_uniform(UInt32(minSpace - CircleButton.MINRAD + 1)) + UInt32(CircleButton.MINRAD))
                    
                    let borders:[Int] = [
                        randX, randY,
                        self.width! + CircleButton.MINRAD * 2 - randX,
                        self.height! + CircleButton.MINRAD * 2 - randY
                    ]
                    
                    for border in borders{
                        if (border < radius){
                            radius = Int(arc4random_uniform(UInt32(border - CircleButton.MINRAD + 1)) + UInt32(CircleButton.MINRAD))
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        if (self.i==59){
                            var k: Int = 0
                            k = self.i
                        }
                        print(String(++self.i)+String(randX) + " " + String(randY) + " " + String(radius))
                        view.addNewCircle(randX, randY: randY, radius: radius)
                    })
                }
                sleep(1)
                //creatingNewCircleOnNextIteration = random() % 2
            }

        })
    }
    

    
}
