//
//  ViewController.swift
//  Simple
//
//  Created by Anton Ivanov on 15.12.15.
//  Copyright © 2015 TonyCo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var i:Int = 0
    private var label: UILabel = UILabel(frame: CGRectMake(0, 0, 200, 21))
    private var circleButtons: QArray?

    private var scoreManager: ScoreManager?


    
     override func viewDidLoad() {
        super.viewDidLoad()

        label.center = CGPointMake(160, 100)
        label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor.brownColor()
        label.text = "I'am a test label"
        self.view.addSubview(label)

        circleButtons = QArray()
        scoreManager = ScoreManager(textView: label)


      _ = CreateCircleButton(view: self, circleList: circleButtons!)

    }
    
    
    internal func addNewCircle(randX: Int, randY: Int, radius: Int){
        let circleButton = CircleButton(radius: radius, x: randX, y: randY)!

        circleButton.setTitle(String(randX)+" "+String(randY)+" "+String(radius), forState: UIControlState.Normal)
        circleButton.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 9)
        circleButton.run(self, scoreManager: self.scoreManager!)

        self.circleButtons!.newArray.append(circleButton)
        self.view.addSubview(circleButton)
    }
    
    internal func removeCircle(circleButton: CircleButton){
        print(circleButtons!.newArray.indexOf(circleButton))
        circleButtons!.newArray.removeAtIndex(circleButtons!.newArray.indexOf(circleButton)!)
        circleButton.removeFromSuperview()
    }


}
