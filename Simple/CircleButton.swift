import Foundation
import UIKit
import AudioToolbox

class CircleButton: UIButton {
    
    internal static let MINRAD: Int  = 25
    internal static let MAXRAD: Int = 75
    
    private let screenSize: CGRect = UIScreen.mainScreen().bounds
    private var x: Int?,  y:Int?, radius:Int?, touched:Int?, score:Int = 0, time:Int = 0
    var label: UILabel = UILabel()


    init?(radius: Int, x: Int, y: Int) {
        super.init(frame: CGRectMake(CGFloat(x - radius), CGFloat(y - radius), CGFloat(radius*2), CGFloat(radius*2)))
        self.radius = radius
        self.x = x
        self.y = y
        //self.addTarget(self, action: "someAction:", forControlEvents: UIControlEvents.TouchUpInside)
        self.layer.cornerRadius = 0.5 * self.bounds.size.width // делает кнопку круглой
        self.backgroundColor = UIColor.redColor()
        //self.setImage(self.drawCircle(), forState: .Normal)
    }
    
    internal func someAction(sender : UITapGestureRecognizer!)
    {
        // определение координат экрана
        //let coordScreen: CGPoint = sender.locationInView(view)
        score++
        ScoreManager(textView: label)
        //label.text = String(score)
    }
    
    
 
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    internal func drawCircle() ->UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 1000, height: 1000), false, 0)
        let context = UIGraphicsGetCurrentContext()
        let rectangle = CGRect(x: 0, y: 0, width: 1000, height: 1000)
        // zalivka
        CGContextSetFillColorWithColor(context, UIColor.blueColor().CGColor)
        CGContextAddEllipseInRect(context, rectangle)
        CGContextDrawPath(context, .FillStroke)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
    
    internal func run(view: ViewController, scoreManager: ScoreManager){
        
        let qualityOfServiceClass = QOS_CLASS_UTILITY
            let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        
            dispatch_async(backgroundQueue, {
                
                
                self.score = self.radius!*1000
                self.time = Int(arc4random_uniform(11) + 1)
                print("Time: " + String(self.time))
                
                while(self.time-- != 0){
                    
                    if((self.touched) != 0){
                        scoreManager.addScore(self.score)
                    }
                    sleep(1)
                    
                }
                view.removeCircle(self)

            })
    }
    

    
    
    internal func getx()->Int {
        return self.x!
    }
    
    internal func gety()->Int {
        return self.y!
    }
    
    internal func getRadius()->Int {
        return self.radius!
    }
    
    


}